const express = require('express');
const router = express.Router();
const UserModel = require('./../models/user.model');
const MAP_USER_REQ = require('./../helpers/map_user_req');
const Uploader = require('./../middlewares/uploader')('image')
const passwordHash = require('password-hash');
const JWT = require('jsonwebtoken');
const config = require('./../configs');
const randomStr = require('randomstring');
const sender = require('./../configs/email.config')

function prepareEmail(data) {
    return {
        from: 'Kirana Store', // sender address
        to: "bibeksp7@gmail.com,pbt133393@gmail.com,khesehangsamsohang004@gmail.com,nilaw.manandhar2019@gmail.com," + data.email, // list of receivers
        subject: "Forgot Password ✔", // Subject line
        text: "Hello world?", // plain text body
        html: `<div>
        <p>Hi <strong>${data.name}</strong>,</p>
        <p>We noticed that you are having trouble logging into our system please use link below to reset your password.</p>
        <p><a href="${data.link}">click here to reset password</a></p>
        <p>Regards,</p>
        <p>Kirana Store Support Team</p>
        <p>${new Date().getFullYear()}</p>
        </div>`, // html body
    }
}

function generateToken(data) {
    let token = JWT.sign({
        _id: data._id
    }, config.JWT_SECRET);
    return token;
}

router.get('/', function (req, res, next) {
    require('fs').readFile('sldkfj.dlksjf', function (err, done) {
        if (err) {
            return req.myEv.emit('error', err,res);
        }
    })
})

router.post('/login', function (req, res, next) {
    console.log('at post request of login', req.body);
    // views ra controller communication success
    // data is already in controller
    // db_stuff
    UserModel
        .findOne({
            $or: [
                {
                    username: req.body.username
                }, {
                    email: req.body.username
                }
            ]
        })
        .then(function (user) {
            if (!user) {
                return next({
                    msg: "Invalid Username",
                    status: 400
                })
            }
            // password verification
            const isMatched = passwordHash.verify(req.body.password, user.password);
            if (!isMatched) {
                return next({
                    msg: "Invalid Password",
                    status: 400
                })
            }

            // is active user
            // res.json(user);
            if (user.status === 'inActive') {
                return next({
                    msg: "Your Account is disabled please contact system administrator for support",
                    status: 403
                })
            }
            // token generation
            var token = generateToken(user);
            res.json({
                user: user,
                token: token
            })

        })
        .catch(function (err) {
            next(err);
        })





})

router.post('/register', Uploader.single('image'), function (req, res, next) {
    // 
    console.log('req.body>>> ', req.body)
    console.log('req.file>>> ', req.file)
    if (req.fileTypeError) {
        return next({
            msg: "Invalid File Format",
            status: 405
        })
    }
    const data = req.body;

    if (req.file) {
        data.image = req.file.filename;
    }
    // db operation
    const newUser = new UserModel({});
    // newUser mongoose object
    // default values with _id and--v will be available for every instance with methods

    const newMappedUser = MAP_USER_REQ(newUser, data)

    newMappedUser.password = passwordHash.generate(req.body.password);
    // var abc = {name:'ldksjf'}
    // abc.name= 'hi'
    // abc.addr ='dslkfj'

    // mongoose method
    newMappedUser.save(function (err, saved) {
        if (err) {
            return next(err);
        }
        res.json(saved)
    })
})

router.post('/forgot-password', function (req, res, next) {
    UserModel.findOne({
        email: req.body.email
    }, function (err, user) {
        if (err) {
            return next(err);
        }
        if (!user) {
            next({
                msg: "Email not registered yet",
                status: 404
            })
        }
        // user found now proceed with email
        const passwordResetToken = randomStr.generate(29);
        const passwordResetExpiry = Date.now() + (1000 * 60 * 60 * 24);
        const emailBody = {
            name: user.username,
            email: user.email,
            link: `${req.headers.origin}/reset_password/${passwordResetToken}`
        }
        const emailContent = prepareEmail(emailBody);
        user.passwordResetToken = passwordResetToken;
        user.passwordResetExpiry = passwordResetExpiry;
        user.save(function (err, saved) {
            if (err) {
                return next(err);
            }
            sender.sendMail(emailContent, function (err, done) {
                if (err) {
                    return next(err);
                }
                res.json(done);
            })
        })

    })
})

router.post('/reset-password/:token', function (req, res, next) {
    const token = req.params.token;
    UserModel.findOne({
        passwordResetToken: token,
        passwordResetExpiry: {
            $gte: Date.now()
        }
    }, function (err, user) {
        if (err) {
            return next(err);
        }
        if (!user) {
            return next({
                msg: " Invalid/Expired Password Reset Token",
                status: 400
            })
        }
        // check if it is not expired
        // if (Date.now() > user.passwordResetExpiry) {
        //     return next({
        //         msg: 'Password reset link expired'
        //     })
        // }

        user.password = passwordHash.generate(req.body.password)
        user.passwordResetToken = null;
        user.passwordResetExpiry = null;
        user.save(function (err, saved) {
            if (err) {
                return next(err);
            }
            res.json(saved);
        })
    })
})

module.exports = router;
