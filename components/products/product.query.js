const ProductModel = require('./product.model');

// db query
// find,
// findById,
// update
// delete
function map_product_req(product, productData) {
    if (productData.name)
        product.name = productData.name
    if (productData.description)
        product.description = productData.description
    if (productData.category)
        product.category = productData.category
    if (productData.brand)
        product.brand = productData.brand
    if (productData.size)
        product.size = productData.size
    if (productData.images)
        product.images = productData.images
    if (productData.price)
        product.price = productData.price
    if (productData.quantity)
        product.quantity = productData.quantity
    if (productData.status)
        product.status = productData.status
    if (productData.modelNo)
        product.modelNo = productData.modelNo
    if (productData.vendor)
        product.vendor = productData.vendor
    if (productData.warrentyStatus)
        product.warrentyStatus = productData.warrentyStatus
    if (productData.warrentyPeriod)
        product.warrentyPeriod = productData.warrentyPeriod
    if (productData.color)
        product.color = productData.color
    if (productData.isReturnEligible)
        product.isReturnEligible = productData.isReturnEligible
    if (productData.manuDate)
        product.manuDate = productData.manuDate
    if (productData.salesDate)
        product.salesDate = productData.salesDate
    if (productData.purchasedDate)
        product.purchasedDate = productData.purchasedDate
    if (productData.returnedDate)
        product.returnedDate = productData.returnedDate
    if (productData.offers)
        product.offers = typeof (productData.offers) === 'string'
            ? productData.offers.split(',')
            : productData.offers
    if (productData.tags)
        product.tags = typeof (productData.tags) === 'string'
            ? productData.tags.split(',')
            : productData.tags
    if (!product.discount)
        product.discount = {};
    if (productData.discountedItem) // x-www-form-url encoded ma boolen value string vayera aaucha
        product.discount.discountedItem = productData.discountedItem
    if (productData.discountType)
        product.discount.discountType = productData.discountType
    if (productData.discountValue)
        product.discount.discountValue = productData.discountValue
}

function find(condition, params = {}) {
    // return new Promise(function (resolve, reject) {
    //     ProductModel
    //         .find(condition)
    //         // skip
    //         // sort
    //         // populate
    //         // limit
    //         .exec()
    //         .then(function (data) {
    //             resolve(data)
    //         })
    //         .catch(function (err) {
    //             reject(err)
    //         })
    // })
    const perPage = params.pageSize ? Number(params.pageSize) : 1000;
    const currentPage = Number(params.pageNumber) || 1;
    const skipCount = perPage * (currentPage - 1);

    return ProductModel
        .find(condition)
        .skip(skipCount)
        .limit(perPage)
        .sort({
            _id: -1
        })
        .populate('vendor', {
            username: 1
        })
        .populate('reviews.user', {
            username: 1
        })
        // limit
        .exec();

}

function insert(data) {
    const newProduct = new ProductModel({});
    map_product_req(newProduct, data);
    return newProduct.save();
}


function update(id, data) {
    return new Promise(function (resolve, reject) {
        ProductModel.findById(id, function (err, product) {
            if (err) {
                return reject(err);
            }
            if (!product) {
                return reject({
                    msg: "Product Not Found",
                    status: 400
                })
            }
            let oldUpdatedImages = [];
            if (data.filesToRemove && data.filesToRemove.length) {
                oldUpdatedImages = remove_existing_images(product.images, data.filesToRemove)
            }

            data.images = oldUpdatedImages
            // if existing images are remove it is now updated
            map_product_req(product, data)

            // push new images with existing images
            if (data.newImages && data.newImages.length) {
                product.images.push(...data.newImages)
            }
            product.save(function (err, updated) {
                if (err) {
                    return reject(err);
                }
                resolve(updated)
            })
        })
    })
}

function remove_existing_images(oldImages = [], filesToRemove = []) {
    let existingImagesCopy = [...oldImages];
    oldImages.forEach(function (image, index) {
        if (filesToRemove.includes(image)) {
            existingImagesCopy.splice(index, 1);
        }
    })
    return existingImagesCopy;

}
function remove(id) {

    return new Promise(function (resolve, reject) {
        ProductModel.findById(id, function (err, product) {
            if (err) {
                return reject(err);
            }
            if (!product) {
                return reject({
                    msg: "Product Not Found",
                    status: 400
                })
            }
            product.remove(function (err, removed) {
                if (err) {
                    return reject(err);
                }
                resolve(removed)
            })
        })
    })
}

function addReview(productId, reviewData) {
    return new Promise(function (resolve, reject) {
        ProductModel.findById(productId, function (err, product) {
            if (err) {
                return reject(err);
            }
            if (!product) {
                return reject({
                    msg: 'Proudct Not Found',
                    status: 404
                })
            }
            product.reviews.push(reviewData);
            product.save(function (err, updated) {
                if (err) {
                    return reject(err);
                }
                resolve(updated)
            })
        })
    })

}

// object shorthand

module.exports = {
    insert,
    find,
    update,
    remove,
    addReview
}



// // two array 
// var aa = ['a', 'b', 'c'];
// var bb = ['a', 'c'];

// function remove_existing_images_1(oldImages = [], filesToRemove = []) {
//     let existingImagesCopy = [...oldImages];
//     oldImages.forEach(function (image, index) {
//         if (filesToRemove.includes(image)) {
//             existingImagesCopy.splice(index, 1);
//             // items ghatyo in oldImages
//             // next splice ma index milena
//         }
//     })
//     return existingImagesCopy;

// }
// remove_existing_images_1(aa, bb);

