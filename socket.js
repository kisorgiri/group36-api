const socket = require('socket.io');
const config = require('./configs')
var users = [];
module.exports = function (app) {
    const io = socket(app.listen(config.SOCKET_PORT), {
        cors: {
            origin: '*'
        }
    })
    io.on('connection', function (client) {
        console.log('socket client connected to server', client.id)
        var id = client.id;
        client.on('new-user', function (name) {
            users.push({
                id: id,
                name: name
            })
            client.emit('users', users);
            client.broadcast.emit('users', users)
        })
        client.on('new-message', function (data) {
            console.log('data is >>',data);
            // console.log("data in new message event", data)
            client.emit('reply-message-own', data);
            // emit is for client who has send connection request
            // for other client we use broadcat.emit()
            client.broadcast.to(data.receiverId).emit('reply-message', data)
            // client.broadcast.emit.to(id).emit('eventName',data)
        })

        client.on('disconnect', function () {
            users.forEach(function (user, index) {
                if (user.id === id) {
                    users.splice(index, 1)
                }
            })

            client.broadcast.emit('users', users)

        })
    })

}
