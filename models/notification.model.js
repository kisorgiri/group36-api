const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const NotificationSchema = new Schema({
    message: {
        type: String,
        required: true
    },
    category: String,
    user_id: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
    seen: {
        type: Boolean,
        default: false
    }
}, {
    timestamps: true
})

module.exports = mongoose.model('notification', NotificationSchema)


// review
// point,
// message
