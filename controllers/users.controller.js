const router = require('express').Router();
const UserModel = require('./../models/user.model');
const MAP_USER_REQ = require('./../helpers/map_user_req');
const Uploader = require('./../middlewares/uploader')();
const NotificationModel = require('./../models/notification.model');
const removeFile = require('./../helpers/remove_file')
// sub route
router.route('/')
    .get(function (req, res, next) {
        //   get all user
        // projection (2nd argument of find) {
        //     username: 1,
        //     _id:0
        // }
        UserModel
            .find({})
            .sort({
                _id: -1
            })
            // .limit(2)
            // .skip(1)
            .exec(function (err, users) {
                if (err) {
                    return next(err);
                }
                res.json(users)
            })
    })
    .post(function (req, res, next) {
        // add user
    });


router.route('/search')
    .get(function (req, res, next) {
        res.json({
            msg: 'from user search'
        })
    })
    .post(function (req, res, next) {

    });


router.route('/:userId')
    .get(function (req, res, next) {
        let id = req.params.userId;
        //    find by id
        UserModel
            .findById(id)
            .then(function (user) {
                if (!user) {
                    return next({
                        msg: "User not found",
                        status: 404
                    })
                }
                res.json(user)
            })
            .catch(function (err) {
                next(err);
            })

    })
    .put(Uploader.single('image'), function (req, res, next) {
        console.log('req.body>>', req.body);
        console.log('req.file >>', req.file)
        if (req.file) {
            req.body.image = req.file.filename;
        }
        // UserModel.findByIdAndUpdate(id,update:{})
        UserModel.findById(req.params.userId, function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: "User Not Found",
                    status: 404
                })
            }
            var oldImage = user.image;
            // if user exist proceed with update
            // user is also mongoose object
            const updatedMapUser = MAP_USER_REQ(user, req.body);

            // once user object is updated with provided value

            updatedMapUser.save(function (err, updated) {
                if (err) {
                    return next(err);
                }

                res.json(updated)
                // server clean up
                if (req.file) {
                    removeFile(oldImage)
                }
                const newNotification = new NotificationModel({});
                newNotification.message = 'Your Profile is updated';
                newNotification.user_id = user._id;
                newNotification.category = 'general';
                newNotification.save();
                // no result handling for now
            })
        })
    })
    .delete(function (req, res, next) {
        // UserModel.findByIdAndRemove(req.params.userId,callback)
        UserModel.findById(req.params.userId, function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: "User Not Found",
                    status: 404
                })
            }
            user.remove(function (err, removed) {
                if (err) {
                    return next(err);
                }
                res.json(removed)
                removeFile(user.image);
            })

        })

    });


module.exports = router;
