// database ==> container that holds data
// harddrive partition jasma programatically database operation perfrom garincha
// database management system DBMS

// DBMS types
// 1. Relational Database Management System (RDBMS)
// 2. Distributed DBMS (Non Relation DBMS)

// 1.Relational Database Management System
// a. table based design
// eg LMS  ==> entities ===> Books,Users,Reviews,Nofications,
// table ==> storage of records
// b. each record are called tuple / row
// c.schema design==> schema validation of properties to be stored in table
// d.relation between table exists
// e.SQL database (structured query language)
// f. types mysql, sql-lite, postgres, mssql,.....
// g.non scalable

// 2.Distributed Database Mangaement System (non-relational)
// a.collection based design
// eg LMS ==> entities  ===> Books, Users
// books,users are collections
// b. each record in a collections are called document
// document based database
// a document can be valid javascript object
// c.schema less design
// no any validation and restriction
// d. highly scalable
// e.relation doesnot exists
// f.NoSQL ==>Not Only SQL
// g.mongodb,couchdb,dynamodb,redis(in memory database)


// eg of books
// var books1 = {
//     name: 'prayogsala',
//     genre: 'polictics',
//     reviews: [
//         {
//             point: 2,
//             message: 'good'
//         },
//         {
//             point: 2,
//             message: 'good'
//         },
//         {
//             point: 2,
//             message: 'good'
//         }
//     ],
//     author:{
//         name:'kishor',
//         dob:'333',
//         username:'jskisor'
//     }
// }


// hint 
// install gari sake pachi
// c: programme files
// mongodb/4.2/bin
// copy that path
// this computer right click ===> properties==> advanced==> environment settings/variable==> 
// system variable
// users variable

// path ==> add/edit
// paste copied url
// apply


// why MONGODB?

// client side (front end) ===> React [JS]
// server side (back end)===> Node/express [JS],
// Database ==> JSON ==> [JS]

// MONGODB
// client - server communication

// mongodob application (server)
// mongod ==> driver initilization
// 27017
// mongodb server
// mongodb://localhost:27017

// http server 
// url ---> http://localhost:8080

// client ko programme
// 1 shell as an client
// 2 UI tool
// 3.Mongod db official driver for nodejs
// 4. ODM tool


// 1. mongo shell
// for connection type mongo command at any location
// once we have > interface our terminal is ready to accept shell command
// shell command
// 1. show dbs  ==> list all the available databases
// to create database
// command => use <db_name>
// if(existing db) select existing database else create new database and select it
// db ==> selected database
// show collections => list all the available collections of selected database

// db operation 
// CRUD operation 
// to add collections C create
// db.<collection_name>.insert({valid json}) either array or object
// db.<collection_name>.insertMany(array) 

// R Read
// db.<collection_name>.find({query_builder})
// db.<collection_name>.find({query_builder}).pretty();// formats output
// db.<collection_name>.find({query_builder}).count();// returns document's count
// db.<collection_name>.find({query_builder}).sort({_id:-1});//decending order (timestamps)
// db.<collection_name>.find({query_builder}).limit(number);//limit result
// db.<collection_name>.find({query_builder}).skip(number);//skip document

// projection  ==> inclusion or exclusion
// eg db.<collection_name>.find({},{key:1[for_inclusion],key:0[for_exclusion]})
// price range $gt,$gte,$lt,$lte

// U update
// db.<collection_name>.update({},{},{});
// 1st object ==> query builder
// 2nd object ==> will have $set as key and value as another object
// another object is payload to be updated
// 3rd object ===> options multi ,upsert

// eg
// db.<collection_name>.update({_id:ObjectId('id')},{$set:{processor:i9}},{multi:true,upsert:true})

// D delete
// db.<collection_name>.remove({condition})
// NOTE don't leave the condition empty


// drop collection
// db.<collection_name>.drop();

// drop database 
// db.dropDatabase();


// ODM || ORM
// ODM ==> Object document modelling (Document based database)
// ORM ==> Object relation mapping (SQL database)


// ODM tool for nodejs and mongodb
// mongoose

// advantages of using mongoose
// 1. Schema based solution
// properties with thier datatypes in a document
// 2. Indexing are lot more easier
// unique,required
// 3. Data type restrictions
// number, boolean, date
// 4. Middleware
// pre hooks
// 5. methods
// find, update,remove, insertOne insertMany
// other then this we have several methods provide by mongoose

// now db connection will be open once server is up
// focus on query only


// BACKUP & RESTORE
// command
// mongodump and mongorestore
// mongoexport and mongoimport

// two way of back and restore
// bson
// human readble JSON and CSV

// bson format

// backup
// command ===> mongodump
// mongodump ==> it will create backup of all the available database under default dump folder
// mongodump --db <selected_db_name> ==> it will back up only selected db under default dump folder

// restore
// command ===> mongorestore

// mongorestore ===> it will look after dump folder and tries to backup database
// mongorestore --drop ===> it will look after dump folder drop existing documents and restore
// mongorestore <path_to_source_folder> ==> checks for bson backup with given folder location

// JSON and CSV

// 1. JSON
// backup 
// command ==> mongoexport
// mongoexport --db <db_name> --collection <collection_name> --out <path_to_destination_folder_with.json_extension>
// mongoexport -d <db_name> -c <collection_name> -o <path_to_destination_folder_with.json_extension>
// --query='{"key":"value",}' 
// restore 6
// command mongoimport
// mongoimport --db <new or exsisting_db> --collection <new or existing collectin> <path_to_source_file with json>


// CSV 
// backup
// command mongoexport
// mongoexport --db <db_name> --collection <collection_name> --type=csv --fileds 'comma,seperated property_name' --out <path_to_destination_with.csv extension>
// mongoexport --db <db_name> --collection <collection_name> --query='{"key":"value",}' --type=csv --fileds 'comma,seperated property_name' --out <path_to_destination_with.csv extension>



// restore
// command mongoimport
// mongoimport --db<db_name> --collection<collection_name> --type=csv <path_to_csv_file> --headerline
 


