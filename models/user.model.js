const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    // db modellling
    firstName: String,
    lastName: {
        type: String
    },
    email: {
        type: String,
        unique: true,
        sparse: true
    },
    contactDetails: {
        mobile: {
            type: Number,
            required: true
        },
        home: Number,
        alternateNumber: Number
    },
    image: String,
    dob: Date,
    gender: {
        type: String,
        enum: ['male', 'female', 'others']
    },
    address: {
        temporaryAddress: [String],
        permanentAddress: String
    },
    username: {
        type: String,
        unique: true,
        required: true,
        lowercase: true,
        trim: true
    },
    password: {
        type: String,
        required: true
    },
    role: {
        type: Number,//1 for admin,2 for normal user,3 visitior
        default: 2
    },
    status: {
        type: String,
        enum: ['active', 'inActive'],
        default: 'active'

    },
    country: {
        type: String,
        default: 'Nepal'
    },
    passwordResetToken: String,
    passwordResetExpiry: Date
}, {
    timestamps: true
})

const UserModel = mongoose.model('user', UserSchema)
module.exports = UserModel;
